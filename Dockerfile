# build stage
FROM golang:alpine AS build
ADD . /src
RUN cd /src && go build -o hello-world

# final stage
FROM alpine
WORKDIR /app
COPY --from=build /src/hello-world /app/
ENTRYPOINT ./hello-world 
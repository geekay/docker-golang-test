package main

import (
	"fmt"
	"net/http"
)

// HelloWorldHandler simply prints out the Hello, World! response
func HelloWorldHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "Hello, World!\n")
}

func main() {
	http.HandleFunc("/", HelloWorldHandler)
	http.ListenAndServe(":3333", nil)
}

# Docker & Golang Multistage Docker Build

A simple golang microservice that runs on port 3333, which is exposed to port 3000 when run from a docker container. This was done using a multistage dockerfile to reduce the final size of the container, simulating a production-like container.

## Steps 

#### 1. Clone the Repository
    git clone https://gitlab.com/geekay/docker-golang-test

#### 2. Run the tests
    go test -v

#### 3. Build the container
    docker build . -t hello-world

#### 4. Run the container
    docker run --rm -p 3000:3333 hello-world
**Note: You may wish to run the command: `docker run --rm -p 3000:3333 -d hello-world` as this will make it so that you can run the subsequent curl command from within the same terminal**

#### 5. Curl to the endpoint
    curl http://localhost:3000/